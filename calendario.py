#!/usr/bin/python3

calendar = {}

def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity

def get_time(atime):
    activities = []
    for date,times in calendar.items():
        for time, activity in times.items():
            if time == atime:
                activities.append((date, time, activity))
    return activities

def get_all():
    all_activities = []
    for date, times in calendar.items():
        for time, activity in times.items():
            all_activities.append((date, time, activity))
    return all_activities

def get_busiest():
    busiest_date = max(calendar, key=lambda k: len(calendar[k]))
    busiest_no = len(calendar[busiest_date])
    return (busiest_date, busiest_no)

def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")

def check_date(date):
    #return True if valid_date_format else False
    import re
    return re.match(r"\d{4}-\d{2}-\d{2}", date) is not None
def check_time(time):
    #return True if valid_time_format else False
    import re
    return re.match(r"\d{2}:\d{2}", time) is not None

def get_activity():
    date = input("Fecha: ")
    while not check_date(date):
        date = input("Formato incorrecto. Introduce la fecha: ")
    time = input("Hora: ")
    while not check_time(time):
        time = input("Formato incorrecto. Introduce la hora: ")
    activity = input("Actividad: ")
    return (date, time, activity)

def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option

def run_option(option):
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)
    elif option == 'B':
        show(get_all())
    elif option == 'C':
        busiest_date, busiest_count = get_busiest()
        print(f"Fecha más ocupada: {busiest_date} ({busiest_count} actividades)")
    elif option == 'D':
        atime = input("Introduce la hora (hh:mm): ")
        show(get_time(atime))

def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)

if __name__ == "__main__":
    main()
    

